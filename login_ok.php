<?php
	//セッションの復元
	  session_start();
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1 id="top">Hogehoge.com</h1>
		
		<div class="welcome_box">
		Welcome <b><?php echo $_SESSION['name']; ?></b>さん
		</div>
	</header>
	
	<!--上部グローバルナビ-->
	<?php include("gl_nav.php"); ?>
	
	
	<?php
		//ログイン確認
		if ($_SESSION['login'] == 'OK') {
		//ログイン成功
		echo 'ログイン中です。';
		echo '<br><br>';
		echo '接続ユーザー：' .$_SESSION['name'];
		} else {
		//ログイン失敗
		echo 'ログインしていません。';
		}
	?>
	
	<br>
	<br>
	
	<a href="login.php">ログイン画面</a><br>
	<a href="logout.php">ログアウト</a>
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

<!--wrapperここまで-->
</body>
</html>
