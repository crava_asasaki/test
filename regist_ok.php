<?php
	//セッションの復元
	session_start();
	//実行
	require_once("php/regist_function.php");
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>

	<div id="content">
	
		<div class="form_wrap">
		
			<?php echo $message; ?>
			
			<div class="center">
				<div class="bottun">
					<a href="login.php">ログイン画面へ</a>
				</div>
			</div><!--centerここまで-->
		
		</div><!--form_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>


</div><!--wrapperここまで-->


<?php
	//セッションの破棄
	 require_once("php/destroy_session.php");
 ?>

</body>
</html>