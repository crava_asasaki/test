<?php
	//セッションの生成
	session_start();
	//実行
	require_once("php/login_function.php");
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<div id="content">
	
		<div id="keijiban_wrap">
		
			<div class="form_wrap">
			
				<span class="red">入力内容が正しくありません。もう一度ご確認の上正しく入力してください。</span>
				
				<form action="login_check.php" method="post">
				
				<h3>ユーザーID</h3>
				<div class="error">
					<?php
					//重複チェック
					echo $empty_regist_error1;
					?>
				</div>
				
				<input type="text" name="user" size="30" value="<?php echo $user; ?>">
				
				<h3>パスワード</h3>
				<div class="error">
					<?php
					//重複チェック
					echo $empty_regist_error2;
					?>
				</div>
				<input type="password" name="pass" size="30" value="<?php echo $pass; ?>">
				
				<div class="center">
					<input type="submit" class="submit" value="ログイン">
				</div>
				</form>
		
			</div><!--form_wrapここまで-->
	

			<div class="center">
				<div class = "box">
					<div class="text">
					当サイトは会員専用掲示板です。<br>
					ご利用するには会員登録（無料）を済ませてください。</div>
					<div class="bottun">
						<a href="regist_user_db.php">新規登録</a>
					</div>
				</div><!--boxここまで-->
				
			</div><!--centerここまで-->
		
		</div><!--keijiban_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->

</body>

</html>