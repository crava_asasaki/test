<?php
	// セッションの復元
	session_start();
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com -新規会員登録-</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<div id="content">
	
		<div class="form_wrap">
		
			<h2>新規会員登録</h2>
			
			<div class="text">
				会員情報を登録してください。<br>
				※機種依存文字は文字化けする可能性がありますので使用しないで下さい。
			</div>
			
			<form action="regist_error_check.php" method="post">
			
			<h3>ニックネーム　(2～10文字以内)</h3>
			<input type="text" name="user_name" size="50" value="<?php echo $_SESSION['user_name']; ?>"/ >
			
			<h3>ログインID　(半角英数字4～16文字)</h3>
			<input type="text" name="login_name" size="50" value="<?php echo $_SESSION['login_name']; ?>" />
			
			<h3>パスワード　(半角英数字4～16文字)</h3>
			<input type="text" name="login_password" size="50" value="<?php echo $_SESSION['login_password']; ?>" />
			
			<div class="center">
			<input type="submit" class="submit" value="確認画面へ">
			</div><!--centerここまで-->    
			</form>
		
		</div><!--formwrapここまで-->
		
		<div class="box">
			<div class="center">
				<div class="text">この掲示板は会員専用です。</div>
				<div class="text"><a href="login.php">ログインはコチラから</a></div>
			</div><!--centerここまで-->
		</div><!--boxここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->

</body>
</html>