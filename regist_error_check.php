<?php
	// セッションの生成
	session_start();
	//実行
	require_once("php/check_db_function.php");
	require_once("php/check_function.php");
	require_once("php/regist_error_check_function.php");
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>Hogehoge.com -新規会員登録-</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery1-9-1.js"></script>
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<div id="content">
	
		<div class="form_wrap">
		
			<h2>新規会員登録</h2>
			
			<div class="text">
				<span class="red">入力内容が正しくありません。もう一度ご確認の上正しく入力してください。</span>
			</div>
			
			<div class="text">
				※機種依存文字は文字化けする可能性がありますので使用しないで下さい。
			</div>
			
			<form action="regist_error_check.php" method="post">
			
			<h3>ニックネーム　(2～10文字以内)</h3>
			
			<div class="error">
			<?php
			//重複チェック
			echo $error_exist1;
			?>
			
			<?php
			//空白、スペースのみでないか
			echo $regist_error1;
			?>
			
			<?php
			//長さチェック
			echo $strlen_error1;
			?>
			
			<?php
			//依存文字チェック
			echo $preg_error1;
			?>
			</div><!--errorここまで-->
			
			<!--ニックネームフォーム部分-->    
			<input type="text" name="user_name" size="35" maxlength="255" value="<?php echo $user_name; ?>" />
			
			
			
			<h3>ログインID　(半角英数字4～16文字)</h3>
			
			<div class="error">
			<?php
			//重複チェック
			echo $error_exist2;
			?>
			
			<?php
			//空白、スペースのみでないか
			echo $regist_error2;
			?>
			
			<?php
			//長さチェック
			echo $strlen_error2;
			?>
			
			<?php
			//依存文字チェック
			echo $preg_error2;
			?>
			</div><!--errorここまで-->
			
			<!--ログインIDフォーム部分-->   
			<input type="text" name="login_name" size="35" maxlength="255" value="<?php echo $login_name_cut; ?>" />
			
			
			
			<h3>パスワード　(半角英数字4～16文字)</h3>
			
			<div class="error">
			<?php
			//空白、スペースのみでないか
			echo $regist_error3;
			?>
			
			<?php
			//長さチェック
			echo $strlen_error3;
			?>
			
			<?php
			//依存文字チェック
			echo $preg_error3;
			?>
			</div><!--errorここまで-->
			
			<!--パスワードフォーム部分-->
			<input type="text" name="login_password" size="35" maxlength="255" value="<?php echo $login_password_cut; ?>" />
			
			<div class="center">
			<input type="submit" class="submit" value="確認画面へ">
			</div><!--centerここまで-->
			</form>
		
		</div><!--form_wrapここまで-->
	
		<div class="box">
			<div class="center">
				<div class="text">この掲示板は会員専用です。</div>
				<div class="text"><a href="login.php">ログインはコチラから</a></div>
			</div><!--centerここまで-->
		</div><!--boxここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->

</body>
</html>