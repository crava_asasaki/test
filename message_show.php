<?php
	// セッションの復元
	session_start();
	// ログインチェック
	require_once("check_login_message.php");
	//リロード対策チケット発行
	$_SESSION['ticket'] = md5(uniqid().mt_rand());
	//データベースへ接続
	require_once("php/connect_db_function.php");
	//実行
	require_once("php/function.php");
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">
<!--入力しなおす　のときに戻ってくるページ-->

	<header>
		<h1 id="top">Hogehoge.com</h1>
		
		<div class="welcome_box">
			Welcome <b><?php echo $_SESSION['name']; ?></b>さん
		</div>
	</header>
	
	<!--上部グローバルナビ-->
	<?php include("gl_nav.php"); ?>
	
	<div id="content">
	
		<div id="keijiban_wrap">
		
			<?php
				echo '<hr class="style-one">';
				while($row=mysql_fetch_object($query)) {
				echo '<h4>' .$row->message_id .':';
				echo $row->user_name;
				echo '(' . $row->entry_date .')' .'</h4>';
				echo '<h5>'.$row->message_title .'</h5>';
				echo '<p>' .nl2br($row->message).'</p>' .'<hr class="style-one">';
				}
				echo '<div id ="least"></div><br>';
			?>
			
			<h2 id="write">メッセージを入力してください。</h2>
			<div class="text">※一度書き込まれたものは、削除・変更できません。</div>
			
			<form action="message_error_check.php" method="post">
			
			<h3>title:</h3>
			<input type="text" name="title" size="50" value="<?php echo $_SESSION['title']; ?>">
			
			<h3> message（必須）：</h3>
			<textarea name="message" cols="40" rows="5" ><?php echo $_SESSION['message']; ?></textarea>
			
			<div class="center">
			<input type="hidden" name="ticket" value="<?php echo $_SESSION['ticket']; ?>">
			<input type="submit" class="submit" name="submit_button" value="書き込む">
			</div><!--centerここまで-->
			</form>
			
			<div class="box"></div>
			
			<hr class="dotted">
			
			<nav id="sub_nav">
				<ul>
					<li><a href="logout.php">Logout</a></li>
					<li><a id="write" href="#top">Page Top</a></li>
				</ul>
			</nav>
		
		</div><!--keijiban_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapper-->

</body>
</html>