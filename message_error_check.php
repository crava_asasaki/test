<?php
	//セッションの復元
	session_start();
	//実行
	require("php/message_error_check_function.php");
	//ログインチェック
	require_once 'check_login_message.php';
	
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">
<!--エラーがあったときは以下を表示、OKのときはmessage_insert.phpへ-->

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<!--上部グローバルナビ-->
	<?php include("gl_nav.php"); ?>
	
	<div id="content">
		
		<div id="keijiban_wrap">
		
			<div class="center"><?php echo $error; ?></div><br>
			
			<div class="center">    
				<div class="bottun"><a href="message_show2.php#write">入力画面に戻る</a></div>
			</div><!--centerとじる-->
		
		</div><!--keijiban_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->

</body>
</html>