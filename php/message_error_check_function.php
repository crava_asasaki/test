<?php
	//htmlspecialchars関数では半角スペースと円記号が変換されないので
	//各々を&nbsp、&yenに置き換える関数
	function hsc($val){
		$s = htmlspecialchars($val , ENT_QUOTES);
		$s = str_replace(' ' , '&nbsp;' , $s);
		$s = str_replace("\\" , '&yen;' , $s);
		return $s;
	}
	
	//タイトル、メッセージ
	$title = hsc($_POST['title']);
	$message = hsc($_POST['message']);
	
	$_SESSION['title'] = $title;
	$_SESSION['message'] = $message;
	
	//リロード対策チケットチェック
	$check_ticket = isset($_POST['submit_button'], $_SESSION['ticket'], $_POST['ticket']) && $_SESSION['ticket'] === $_POST['ticket']; 
	
	if ($check_ticket) {

		//空白、スペースのみではないかのチェック
		$pattern="^(\s|　)+$";  //正規表現のパターン
		
		if(empty($message) || (mb_ereg_match($pattern,$str))){
		
		//空白のとき
		$error = '<div class="red">本文が入力されていないと書き込みはできません。入力しなおしてください。</div>';
	
		//セッションを破棄
		unset($_SESSION['ticket']);
		}
	}

	//エラーが無ければページ遷移
	if(empty($error)){
		header('Location: message_agree.php');
		}
?>