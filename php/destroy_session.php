<?php
	//セッション変数を初期化
	$_SESSION = array();
	
	//セッションIDを破棄
	If (isset($_COOKIE[session_name()])) {
		setcookie(session_name(), '', time()-3600, '/');
	}
	
	//セッションを破棄
	session_destroy();
?>