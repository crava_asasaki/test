<?php
	//regist_error_check の実行部分
	
	//名前、ID、パス
	$user_name = htmlspecialchars($_POST['user_name'], ENT_QUOTES);
	$user_name_cut = trim(mb_convert_kana( $user_name, "s"));//半角に
	
	$login_name = htmlspecialchars($_POST['login_name'], ENT_QUOTES);
	$login_name_cut = trim(mb_convert_kana( $login_name, "sa"));//半角に、前後スペースカット
	
	$login_password = htmlspecialchars($_POST['login_password'], ENT_QUOTES);
	$login_password_cut = trim(mb_convert_kana( $login_password, "sa"));//半角に、前後スペースカット
	
	$_SESSION['user_name'] = $user_name;
	$_SESSION['login_name'] = $login_name;
	$_SESSION['login_password'] = $login_password;

	//リロード対策チケット発行
	$_SESSION['ticket'] = md5(uniqid().mt_rand());

	//各項目の空白、スペースチェック
	$regist_error1 = empty_error($user_name_cut);
	$regist_error2 = empty_error($login_name_cut);
	$regist_error3 = empty_error($login_password_cut);
	
	//各項目の長さチェック
	$strlen_error1 = strlen_error($user_name,10,2);
	$strlen_error2 = strlen_error($login_name,16,4);
	$strlen_error3 = strlen_error($login_password,16,4);

	//各項項目機種依存文字チェック
	require('PlatformDependentChars.php');
	$preg_error1 = izonmoji($user_name);
	$preg_error2 = izonmoji($login_name);
	$preg_error3 = izonmoji($login_password);
	
	//重複チェック（IDと名前）
	$error_exist1 = error_exist($user_name,"user_name");
	$error_exist2 = error_exist($login_name,"login_name");
	
	//エラーが無ければページ遷移
	if((empty($regist_error1)) && (empty($regist_error2)) && (empty($regist_error3)) && (empty($strlen_error1)) && (empty($strlen_error2)) && (empty($strlen_error3)) && (empty($preg_error1)) && (empty($preg_error2)) && (empty($preg_error3)) && (empty($error_exist1)) && (empty($error_exist2))){
	header('Location: regist_agree.php');
	}
?>