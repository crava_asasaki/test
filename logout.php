<?php
	//セッション開始
	session_start();
	//セッションの破棄
	 require_once("php/destroy_session.php");
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<div class="clearfix"></div>
	
	<div id="keijiban_wrap">
	
		<div class="center">
			<div class="text">ログアウトしました。</div>
			
			<div class="bottun">
				<a href="login.php">ログイン画面へ</a>
			</div>
		</div><!--centerここまで-->
	
	</div><!--keijiban_wrapここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>
	
</div><!--wrapperここまで-->
</body>
</html>