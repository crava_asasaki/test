<?php
	//セッションの復元
	session_start();
	
	//ログインチェック
	if ($_SESSION['login'] != 'OK'){
	
	//ログインしていないメッセージを表示する
		
echo <<<EOT

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<div id="content">
	
		<div id="keijiban_wrap">
		
			<div class="center">
			
				<div class = "box">
				
					<div class="text">Hogehoge.comは会員専用掲示板です。<br>
					書き込み・閲覧を行うにはログインしてください。
					</div>
					
					<div class="bottun">
						<a href="login.php">ログイン</a>
					</div>
					
					</div><!--boxここまで-->
					
					
					<div class = "box">
					
					<div class="text">まだ会員でない方は会員登録（無料）を済ませてください。
					</div>
					
					<div class="bottun">
						<a href="regist_user_db.php">新規登録</a>
					</div>
				
				</div><!--boxここまで-->
			
			</div><!--centerここまで-->
		
		</div><!--keijiban_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->


</body>
</html>

EOT;
//終了
exit();
	}