<?php
	//セッションの復元
	session_start();
	//ログインチェック
	require_once 'check_login_message.php';
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<!--上部グローバルナビ-->
	<?php include("gl_nav.php"); ?>
	
	<div id="content">
	
		<div id="keijiban_wrap">
			<h2>書き込み内容を確認してください。</h2>
				<div class="red">
					上記の内容で書き込んでよろしいですか？
					書き込み後は、削除・編集はできません。
				</div><!--redここまで-->
				
				<?php echo $test ?>
				
				<!--確認表示部分-->
				<h3>title</h3>
				<div class="textbox">
					<?php echo $_SESSION['title']; ?>
				</div>
				
				<h3>message</h3>
				<div class="textbox">
				<?php
					// 改行部分にBRタグを埋め込む
					echo nl2br($_SESSION['message']);
				?>
				</div>
				<br>

			<div class="center">
				<form action="message_insert.php" method="post">
				<input type="hidden" name="ticket" value="<?php echo $_SESSION['ticket']; ?>">
				<input type="submit" class="submit" name="submit_button" value="この内容で送信する">
				<div class="bottun"><a href="message_show.php#write">入力画面に戻る</a></div>
				</form>
			</div><!--centerとじる-->
				
		</div><!--keijiban_wrapここまで-->
		
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>
    
</div><!--wrapperここまで-->

</body>
</html>