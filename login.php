<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com -ログイン-</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<div id="content">
	
		<div class="form_wrap">
		
			<h2>ユーザー名とパスワードを入力してください。</h2>
			
			<form action="login_check.php" method="post">
			
			<h3>ユーザーID</h3>
			<input type="text" name="user" size="30">
			
			<h3>パスワード</h3>
			<input type="password" name="pass" size="30">
			
			<div class="center">
				<input type="submit" class="submit" value="ログイン">
			</div>
			</form>
	
		</div><!--form_wrapここまで-->
	
	<div class="box">    
		<div class="center">
			<div class="text">Hogehoge.comは会員専用掲示板です。</div>
			<div class="text"><a href="regist_user_db.php">会員登録（無料）はコチラから</a></div>
		</div><!--centerここまで-->
	</div><!--boxここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->

</body>
</html>
