<?php
	//セッションの復元
	session_start();
	//実行
	require_once("php/message_insert_function.php");
	//ログインチェック
	require_once("check_login_message.php");
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">

<SCRIPT language="JavaScript">
	<!--
	// 一定時間経過後に指定ページにジャンプする
	mnt = 3; // 何秒後に移動するか
	url = "message_show.php"; // 移動するアドレス
	function jumpPage() {
	  location.href = url;
	}
	setTimeout("jumpPage()",mnt*1000)
	//-->
</SCRIPT>

</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>    
	
	<!--上部グローバルナビ-->
	<?php include("gl_nav.php"); ?>
	
	<div id="content">
	
		<div id="keijiban_wrap">
		
			<!--書き込み完了文orエラー文-->
			<div class="text"><?php echo $error; ?></div>
			<div class="text"><?php echo $ok_message; ?></div>
			<br>
			
			<div class="text">ページが自動で書き込みページへ切り替わります。</div>
			
			<div class="text">
				しばらくたっても切り替わらない場合は
				<a href="message_show.php">コチラをクリックしてください。</a>
			</div>
		
		</div><!--keijiban_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->

<?php
	//セッションを破棄
	unset($_SESSION['ticket']);
	unset($_SESSION['title']);
	unset($_SESSION['message']);
?>

</body>
</html>