<?php
	//セッションの復元
	session_start();
	
	// ログインチェック
	require_once 'check_login_message.php';
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
		
		<div class="welcome_box">
			Welcome <b><?php echo $_SESSION['name']; ?></b>さん
		</div>
	</header>
	
	<!--上部グローバルナビ-->
	<?php include("gl_nav.php"); ?>
	
	<div id="content">
	
		<div class="clearfix"></div> 
		
		<div id="keijiban_wrap">
		
			<h2>メッセージを入力してください。</h2>
			
			<form action="message_insert.php" method="post">
			<h3> title：</h3>
			<input type="text" name="title" size="50">
			
			<h3>message(必須)：</h3>
			<textarea name="message" cols="40" rows="5"></textarea>
			
			<div class="center">
			<input type="submit" class="submit" value="メッセージの登録">
			</div>
			</form>
		
		</div><!--keijiban_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->

</body>
</html>