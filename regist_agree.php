<?php
// セッションの復元
session_start();
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8">
<title>Hogehoge.com -新規会員登録-</title>
<link href="css/html5reset-1.6.1.css" rel="stylesheet" type="text/css">
<link href="css/base.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">

	<header>
		<h1>Hogehoge.com</h1>
	</header>
	
	<div id="content">
	
		<div class="form_wrap">
		
			<h2>ご登録内容の確認</h2>
			
			<h3>ニックネーム</h3>
			<div class="textbox">
			<?php echo $_SESSION['user_name']; ?>
			</div>
			
			<h3>ログインID</h3>
			<div class="textbox">
			<?php echo $_SESSION['login_name']; ?>
			</div>
			
			<h3>パスワード</h3>
			<div class="textbox">
			<?php echo $_SESSION['login_password']; ?>
			</div>
			
			<div class="center">
			
			<div class="red">上記の内容で登録してよろしいですか？</div>
			
			<form action="regist_ok.php" method="post">
			<input type="hidden" name="ticket" value="<?php echo $_SESSION['ticket']; ?>">
			<input type="submit" class="submit" name="submit_button" value="この情報で会員登録する">
			</form>
			
			<div class="bottun">
				<a href="regist_user_db.php">入力しなおす</a>
			</div>
			
			</div>
		
		</div><!--form_wrapここまで-->
	
	</div><!--contentここまで-->
	
	<!--下部フッター-->
	<?php include("footer.php"); ?>

</div><!--wrapperここまで-->   
</body>
</html>
	
